echo ----------
echo $(date)

### Go to directory with cloned git repo
cd ~/deploy_behaviorchange.opens.science

### Delete old 'public' directory if it exists
#rm -rf public

pwd
echo $PATH
echo Calling PkgDown

### Render the site
#/usr/local/bin/quarto render --to all
/usr/local/bin/R -e "pkgdown::build_site();"

echo Finished PkgDown

### Copy image
#mkdir public/img
#cp img/hex-logo.png public/img/hex-logo.png

### Delete all contents in public HTML directory
rm -rf ~/behaviorchange.opens.science/*.*
rm -rf ~/behaviorchange.opens.science/*
rm -f ~/behaviorchange.opens.science/.htaccess

### Copy website
cp -RT public ~/behaviorchange.opens.science

### Copy .htaccess
cp -f .htaccess ~/behaviorchange.opens.science

echo ----------
