
# This file is a generated template, your changes will not be overwritten

dMCDjmvClass <- if (requireNamespace("jmvcore", quietly=TRUE)) R6::R6Class(
    "dMCDjmvClass",
    inherit = dMCDjmvBase,
    private = list(
        .run = function() {

            # `self$data` contains the data
            # `self$options` contains the options
            # `self$results` contains the results object (to populate)

            cer <- jmvcore::toNumeric(self$options$cer) / 100;
            mcd <- jmvcore::toNumeric(self$options$mcd) / 100;

            if (is.numeric(cer) && (cer > 0) && (cer < 1) &&
                is.numeric(mcd) && (mcd > 0) && (mcd < 1)) {

                res <-  dMCD(cer=cer, mcd=mcd, plot=FALSE);

                self$results$dMCDplot$setState(list(cer=cer,
                                                    mcd=mcd));

                self$results$text$setContent(paste0("For a Control Event Rate (CER) of ", cer, "\n",
                                                    "and a Meaningful Change Definition (MCD) of ", mcd,
                                                    ",\nCohen's d is ", round(res, 3), "."));

            } else {
                self$results$text$setContent(paste0("Enter a Control Event Rate (CER, i.e. base rate) and a ",
                                                    "Meaningful Change Definition (MCD), both as percentages ",
                                                    "(i.e. as numbers between 0 and 100)."));
            }

        },
        .plot = function(dMCDplot, ggtheme, theme, ...) {
            if (!is.null(dMCDplot$state)) {
                cer <- dMCDplot$state$cer;
                mcd <- dMCDplot$state$mcd;
                if (is.numeric(cer) && (cer > 0) && (cer < 1) &&
                    is.numeric(mcd) && (mcd > 0) && (mcd < 1)) {
                    res <-  dMCD(cer=cer, mcd=mcd, plot=TRUE);
                    res <- attr(res, "plot") +
                        ggplot2::theme(panel.background = ggplot2::element_rect(fill="transparent",
                                                                                color="transparent"),
                                       plot.background = ggplot2::element_rect(fill="transparent",
                                                                               color="transparent"),
                                       legend.background = ggplot2::element_rect(fill="transparent",
                                                                                 color="transparent"),
                                       legend.box.background = ggplot2::element_rect(fill="transparent",
                                                                                     color="transparent")) +
                        # ggtheme + ggplot2::theme(plot.title = ggplot2::element_text(margin=ggplot2::margin(b = 5.5 * 1.2)),
                        #                          plot.margin = ggplot2::margin(5.5, 5.5, 5.5, 5.5)) +
                        NULL;
                    print(res);
                }
            }
            TRUE;
        })
)
